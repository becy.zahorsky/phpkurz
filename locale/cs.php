<?php

$locale = [
    'work.title' => 'Moje pracovní zkušenosti',
    'work.perex' => 'Všechny moje dosavadní pracovní zkušenosti přehledně na jednom místě. Ráda pracuji na projektech, které mají smysl. Déle u nich vydržím.',
];
