<?php
include_once 'config/config.php';
include_once 'lib/functions.php';
?>

<!DOCTYPE html>
<html lang="en">

    <?php
    // vlož a vykonej soubor head.php ve složce layout
    include_once 'layout/head.php';
    ?>

    <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
        <div class="site-wrap">

            <?php
            // hlavicka
            include_once 'layout/header.php';

            // obsah
            include_once 'page/homepage/base.php';

            // paticka
            include_once 'layout/footer.php';
            ?>

        </div> <!--.site-wrap -->

        <?php
        include_once 'layout/scripts.php';
        ?>
    </body>
</html>