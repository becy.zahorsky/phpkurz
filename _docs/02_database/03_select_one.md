# Zobrazení jednoho

## Úvod

Udělali jsme jednoduchý výpis článků, který ještě trochu vylepšíme, ale hlavně, chtěli bychom si celý článek přečíst. K tomu potřebujeme vytvořit novou stránku, nebo upravit stávající, aby se podle URL zobrazil konkrétní článek. Z výpisu na něj pak budeme odkazovat.

## Dluhy z minula

1. Při volání `fetchAll()` použít konstantu `PDO::FETCH_ASSOC`. Bude se nám pak pole vracet jenom s klíči, které nás zajímají.
2. Do proměnné `$menu` si přidáme novou položku, abychom měli odkaz na blog.
3. Co konečně opravit odkaz na Homepage?
4. Nějaké to stylování, je to teď celé rozbombené!

Do `style.css` přidáme:

```css
.intro-section-subpage {
  height: 90px;
  position: relative; 
}
.intro-section-subpage:before {
  content: "";
  position: absolute;
  height: 100%;
  width: 70%;
  background: #007bff;
  border-bottom-right-radius: 0px; 
}
@media (max-width: 991.98px) {
  .intro-section-subpage:before {
    width: 100%; 
  } 
}
```

Šablona podstránky bude vždycky začíant takto:
```html
<div class="intro-section-subpage"></div>
```

No samotný obsah by taky chtěl nějaké lepší formátování:
```html
<div class="mt-5 mb-5"> <!-- nebo site-section -->
  <div class="container">
    <div class="row">
      <div class="col-lg-6 mb-5">
        <span class="section-sub-title d-block">podnadpis</span>
        <h2 class="section-title">Nadpis stránky</h2>
        
        <!-- tady bude obsah stránky -->
          
      </div>
    </div>
  </div>
</div>
```

## Úkol

1. Potřebujeme naučit controller `blog` reagovat na `GET` parametr `id`. Bude používat jinou šablonu, jestli jde o výpis článků nebo detail jednoho. Jak?
2. Zeptáme se na obsah `GET` a podle toho nastavíme proměnnou `$template`
```php
if ($_GET['id'] !== null) {
	$template = 'blog-detail';
} else {
	$template = 'blog';	
}
```
3. Ale fuj.
4. Co takhle?
```php
$idClanku = $_GET['id'] ?? null;

$dotaz = $pdo->prepare("SELECT * FROM clanky WHERE id = :id");
$dotaz->execute(['id' => $idClanku]);
$clanek = $dotaz->fetch(PDO::FETCH_ASSOC);

if ($clanek !== false) {
	$template = 'blog-detail';
} else {
	$template = 'blog';	
}
```
5. Skoro.
6. Ještě musíme myslet na to, že uživatele nesmíme nechat na špatné adrese:
```php
$idClanku = $_GET['id'] ?? null;

if ($idClanku !== null) {
	$dotaz = $pdo->prepare("SELECT * FROM clanky WHERE id = :id");
	$dotaz->execute(['id' => $idClanku]);
	$clanek = $dotaz->fetch(PDO::FETCH_ASSOC);	

	if ($clanek !== false) {
		$template = 'blog-detail';		
	} else {
		header('location: ?page=blog');
		exit();
	}
} else {
	$template = 'blog';
	// nacteni vsech clanku
}
```
7. Teď můžeme v nové šabloně vypsat celý obsah jednoho konkrétního článku, který máme připravený v proměnné `$clanek`.
