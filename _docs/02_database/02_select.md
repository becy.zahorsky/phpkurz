# Čtení z databáze

## Úvod

Nejčastěji budeme chtít z databáze číst data, která tam máme uložená. Vytvoříme si jednoduchou stránku `Blog`, kde budeme psát svoje zážitky nebo o našich koníčcích. Třeba o programování.

## Úkol
1. Vytvoříme novou stránku, tzn. nejdřív `controller/blog.php`, ve kterém nastavíme proměnnou `$template = 'blog';` a potom samotnou šablonu `page/blog/base.php`, resp. `/page/blog.php`, kde budeme články vypisovat.
2. V controlleru položíme dotaz na všechny články, které v databázi máme:
```php
// Připravení dotazu
$dotaz = $pdo->prepare("SELECT * FROM clanky");
// Vykonání dotazu
$dotaz->execute();
// a nakonec získání všech řádků, které nám vrací (jen u SELECT)
$clanky = $dotaz->fetchAll();
```
3. Ověříme si pomocí `var_dump()`, co se nám do proměnné `$clanky` uložilo.
4. Načtená data zpracujeme standardním způsobem.
5. Články chceme načítat od nejnovějšího, takže přidáme do dotazu řazení:
```sql
SELECT * FROM clanky ORDER BY id desc
```
6. Napadají tě nějaké další sloupce, které chceš do tabulky přidat?

## Co dál?

Můžeme si vyzkoušet, jak převést nějaká naše existující data z `config.php` do databáze, když už umíme vytvořit tabulku a načíst z ní nějaká data. Proč né rovnou všechna. Máme výpis fotogalerie a výpis pracovních zkušeností.

### Fotogralerie

Data/pole pro fotogalerii vypadá nějak takto:

```php
$galerie = [
    [
        'cesta' => 'Svatba_0055.jpg',
        'titulek' => 'Náš velký den',
        'popis' => '...dlouhy text...'
    ],
    [
        'cesta' => 'DSC_0022.jpg',
        'titulek' => 'První dovolená',
        'popis' => '...dlouhy text...',
    ],
    // a dalsi fotky
];
```

Z toho nám vyplývá, že nová tabulka v databázi, pojmenujeme ji `fotky`, bude mít tyto kolonky:
```sql
id int auto increment
cesta varchar(255)
titulek varchar(255) NULL
popis text NULL
```

U položek, kde máme povolený `NULL` (prázdná hodnota), zaškrtneme při vytváření v Admineru.

SQL kód vytvoření tabulky bude vypadat nějak takto:

```sql
CREATE TABLE `fotky` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `cesta` varchar(255) NOT NULL,
  `titulek` varchar(255) NULL,
  `popis` text NULL
);
```

Pomocí Admineru ručne vytvoříme záznamy v tabulce podle našeho původního pole v `config.php`.

Teď můžeme v controlleru pro `homepage.php` položit jednoduchý SQL dotaz na získání všech fotografií.

```php
$dotazFotky = $pdo->prepare('SELECT * FROM fotky ORDER BY id DESC');
$dotazFotky->execute();
$fotky = $dotazFotky->fetchAll(PDO::FETCH_ASSOC);
```

Teď mám v šabloně k dispozici nově proměnnou `$fotky`, kde mám načtené všechny fotografie z databáze. Kontrolním výpisem (`var_dump($fotky);exit;`) si ověřím strukturu:
```
array(2) {
  [0]=>
  array(4) {
    ["id"]=>
    string(1) "2"
    ["cesta"]=>
    string(12) "DSC_0022.jpg"
    ["titulek"]=>
    string(16) "První dovolená"
    ["popis"]=>
    NULL
  }
  [1]=>
  array(4) {
    ["id"]=>
    string(1) "1"
    ["cesta"]=>
    string(14) "Svatba_055.jpg"
    ["titulek"]=>
    string(13) "nas velky den"
    ["popis"]=>
    NULL
  }
}
```

Vidíme, že máme pole krásně odpovídající řádkům v databázi a vnořené pole už jsou konkrétní záznamy, kde máme klíč k hodnotě ve stejném názvu jako je sloupec v databázi.

Proč se nám tady dotaz vrací pole jen s klíči podle sloupců? Uvedli jsme jako vstupní parametr metody `fetchAll()` konstantu `PDO::FETCH_ASSOC`, pomocí které konektoru do databáze řekneme, že to chceme mít jako asociativní pole (klíč odpovídá názvu sloupce). Můžeme si tuto konstantu přidat do našeho původního dotazu v `blog.php` controlleru.

Takové pole jednoduše projdeme `foreach`em. Ten už v kódu máme, takže stačí v něm použít novou proměnnou `$fotky`. Pokud jsme zachovali strukturu podle našeho původního pole, tak nebude potřeba nic upravovat.

Nezapomeneme smazat původní proměnnou v `config.php`, kde se nám to začne pěkně čistit.

### Pracovní zkušenosti

Stejným způsobem, jako jsme převedli fotogralerii, můžeme do databáze přenést pracovní zkušenosti. Doporučil bych ale pár zjednodušení, protože pokud bychom chtěli pole ve stejné struktuře, museli bychom si data uložit ve více tabulkách a vytvořit si nějaké relace. To necháme na SQL kurz.

Zjednodušil bych to na následující formát:

```php
$pracovniZkusenosti = [
    [
        'jmeno' => 'AVG',
        'delka' => 1,
        'od' => '2016-03-01',
        'do' => '2016-07-31'
        'popis' => 'Tester antivirového programu.',
        'kolegove' => 'Tomáš, Petr, Franta',
        'nadrizeny' => 'Petr Vomáčka',
    ],
    // ...
];
```

Rozdíl je ten, že kolegové a nadřízený je jako textová hodnota, už ne další zanořené pole.

Tabulka `pracovni_zkusenosti` by tedy potom vypadala takto:

```sql
CREATE TABLE `pracovni_zkusenosti` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `jmeno` varchar(255) NOT NULL,
  `delka` smallint NOT NULL, -- na pocet let nam staci male cislo
  `od` date NULL, -- specialni typ na ulozeni datumu
  `do` date NULL,
  `popis` text NOT NULL,
  `kolegove` varchar(255) NOT NULL,
  `nadrizeny` varchar(255) NOT NULL
);
```

V poznámkách u sloupců, které mají nějaký nový tip k němu máte pár informací.

Další postup je stejný. Po načtení dat z databáze a uložení si do proměnné musíme upravit náš výpis pracovních pozic, protože jsme zjednodušili strukturu. Takže vnořený cyklus už nebude potřeba.

Já bych ho pro studijní účely nemazal, ale jen zakomentoval. Stejně tak původní pole. Nechal bych tam ale třeba jen jednu položku.
