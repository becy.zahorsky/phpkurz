# Připojení k databázi

## Úvod
Abychom mohli pracovat s databází, musíme se k ní umět připojit, ale v první řadě musí nějaká databáze existovat, musíme k ní mít oprávnění a umět v ní tvořit tabulky.

Nejjednodušší je použít na to nějaký nástroj na správu databáze. Hojně používaný, ale nic moc je PHPMyAdmin, lepší je Adminer (adminer.org). Lépe se v něm orientuje a umí toho víc.


## Úkol
1. V terminálu spustíme `mysql -u root`
2. Zadáme příkaz `SET PASSWORD FOR 'root'@'localhost' = PASSWORD("root");` kde root v PASSWORD je nové heslo uživatele `root`.
3. Stáhneme Adminer a umístímě ho do složky `adminer` ve složce `htdocs` v naší instalaci XAMPP. Stažený soubor tam překopírujeme jako `index.php`.
4. Otevřeme adresu http://localhost/adminer a uvidíme přihlašovací obrazovku
5. Vyplníme
	server: localhost
	uživatel: root
	heslo: root
6. Pomocí odkazu `Vytvořit databázi` vytvoříme databázi s názvem `mujweb` a porovnáním `utf8mb4_unicode_ci`.
7. Pomocí odkazu Vytvořit tabulku si vytvoříme následující tabulku `clanky`:
```
id int AI  
nazev varchar(256)  
perex tinytext  
obsah text  
```
8. Pomocí odkazu `Nová položka` vložíme svůj první záznam. Nejlépe si vytvoříme alespoň 3, ať se nám s tím dobře pracuje při čtení.
9. Adminer nám ukazuje (na klik) poslední položený dotaz do databáze.
10. Vytvoříme si ve složce `config` nový soubor `connection.php` vložíme následující kód
```php
// Připojovací údaje
define('SQL_HOST', 'localhost');
define('SQL_DBNAME', 'mujweb');
define('SQL_USERNAME', 'root');
define('SQL_PASSWORD', 'root');

$dsn = 'mysql:dbname=' . SQL_DBNAME . ';host=' . SQL_HOST . ';charset=utf8mb4';
$user = SQL_USERNAME;
$password = SQL_PASSWORD;

try {
    $pdo = new PDO($dsn, $user, $password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    die('Connection failed: ' . $e->getMessage());
}
```

11. Pokud se připojení povedlo, uvidíme stránku tak jako jsme ji viděli dosud. Jinak uvidíme chybovou hlášku, že se připojení nepovedlo.