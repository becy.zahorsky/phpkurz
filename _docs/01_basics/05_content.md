# Tvorba obsahu

## Úvod

Stránka už se začíná pomalu plnit, ale ještě pořád jsou místa, kde máme původní texty ze šablony nebo nám zůstal obsah, který už jsme na jiných místech udělali dynamicky. Např. hlavní menu. Nezapomněli jsme ho ale udělat taky v patičce?

## Úkol
1. Zamyslíme se, jak by web šel udělat více jazyčně. Nechceme psát všude podmínky, jaký text zobrazit.
2. Potřebujeme k tomu nějakou chytrou funkci, nazveme ji `t` jako `translate`.
3. Její definice bude vypadat nějak takto:
```php
function t(string $key): string {
  global $locale;
  
  return $locale[$key] ?? $key;
}
```
4. Vytvoříme si soubor `config/cs.php`, kam budeme ukládat v nové proměnné `$locale` všechny naše texty. Soubor nesmíme zapomenout připojit v našem souboru `index.php` nebo ještě lépe `config.php` na začátku (pokud máš podle mojí struktury, tak ještě lépe `bootstrap.php`).
5. Vyzkoušíme volání překladu:
```php
<p><?= t('omne.nadpis') ?></p>
```
6. Nakonec vytvoříme také soubor `config/en.php`, pro anglickou jazykovou mutaci a při připojování souboru s texty použijeme podmínku:
```php
$cesky = true;

if ($cesky === true) {
	include_once('config/cs.php');
} else {
	include_once('config/en.php');	
}
```
7. Teď se nám web bude načítat česky. Pokud změníme hodnotu proměnné `$cesky` na `false`, bude se načítat s anglickými texty.

## Co dál?

1. Nahradíme na stránce texty za překladové řetězce. Vyber si alespoň dva celé bloky, kde nahraď původní texty ze šablony svými vlastními a použij k tomu překladové řetězce s voláním funkce `t()`.
2. V patičce nahraď menu za volání funkce, která stejně tak jako v hlavním menu, ho vykreslí. Využij kombinaci funkcí. V minulém úkolu jsme menu upravili tak, že jedna funkce se stará o menu jako celek a druhá se stará jen o položku. Zkus využít funkci tak, aby si neduplikovala svůj kód.
3. Zopakuj si základy v aplikaci (SoloLearn)[https://www.sololearn.com/Course/PHP/] v modulu 1 až 6. Pošli screen, kam až dojdeš :-)
