# Dynamické změny obsahu na webu a použití funkcí

## Úvod
Ukážeme si praktické použití funkcí a konstant pro vytvoření prvního 
dynamického obsahu.

## Úkol
1. Vytvoříme si soubor **functions.php**,
do kterého si budeme vkládat všechny funkce, které budeme nadále používat na 
našem webu. Tento soubor si připojíme do našeho hlavního souboru 
**index.php** pomocí příkazu `include_once`.
2. Jako první si zde ale vytvoříme konstantu `NAZEV_WEBU`,
ve které si nastavíme obecný název pro náš web a který budeme všude používat.
Konstanty se vytváří pomocí funkce `define()`.
3. K této konstantě si zaráz vytvoříme i funkci `vratNazevWebu()`,
která nám bude získávat a vracet obsah této konstanty.
4. Nyní, když máme vytvořenou tuto funkci,
nahradíme staticky definovaný text ve všech částech webu pomocí této funkce.
Výskyt takového textu můžeme najít v souborech **header.php**,  **head.php**, 
**footer.php** a **menu.php**.
5. Vytvor novou proměnnou `$menu`, která bude pole o tolika položkách, kolik jich máme
v hlavním menu. Každá položka bude mít klíč `odkaz` a `nazev`, abychom si k ní uložili
všechno co má.
6. Vytvoříme funkci `vypisMenu($menu)`, která nám ho vrátí jako složený HTML výstup. Funkce má vstupní parametr
pole položek.

## Co dál?

1. Jakožto praví programátoři se ale s funkcí `vypisMenu()` nemůžeme 
jen tak spokojit. Tato funkce sama o sobě stále provádí dvě logicky různé 
věci: prochází položky menu a generuje samotné jednotlivé odkazy.
Zkusme si tedy samotné sestavování `<a href...` elementu vyseparovat do další 
malé funkce s názvem `vypisPolozkuMenu()`. Nová funkce by mohla přijímat dva
parametry: 1) odkaz položky 2) zobrazovaný název položky
Tuto funkci potom použijeme ve funkci `vypisMenu()` na vhodném místě.

2. V souboru **footer.php** máme ještě staticky napsaný rok, kdy jsme naše
stránky vyrobili. Chěli bysme ale, aby se nám v průběhu času formát rošířil
na rozpětí let, kdy jsme web udržovali. Takže když bude rok vzniku webu 2017,
tak v patičce chceme vypsat: 2017-2019. 2019 je aktuální rok, který se nám
bude každý rok automaticky měnit. Vytvoříme si tedy funkci, kam budeme posílat
rok, kdy jsme web vyrobili a vrátí se nám správný text. Funkci pak použij v patičce.

3. Úplne definice funkce zní: funkce přijímá povinný parametr typu číslo. Výstup je následující:
	a) pokud je vstupní rok stejný jako aktuální rok, výstup bude stejný jako vstup
	b) pokud je vstupní rok menší než aktuální rok, výstup je `{{vstupní-rok}}-{{aktualni-rok}}`
	c) pokud je vstupní rok větší než aktuální rok, výstup je aktuální rok

4. Jak zjistit aktuální rok ti pomůže zjistit Google :-)

5. Rok vytvoření webu si dáme do poměnné, kterou dáme klasicky do `config.php`, ať je máme na jednom
místě.