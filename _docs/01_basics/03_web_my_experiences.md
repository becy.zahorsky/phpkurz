# Výpis pracovních zkušeností

## Úvod

V této částo se zaměříme na procvičení polí, cyklů a podmínek.

## Úkol
1. Prvním úkolem bude vytvořit pole `$pracovnizkusenosti` obsahující všechny moje 
pracovní zkušenosti. To znamená vytvořit pole, skládající se z položek 
typu `string`.
2. Každá z těchto položek bude nějaká pracovní zkušenost.
Takové pracovní zkušenosti si vytvořte minimálně tři.
3. V části **services-section** si tyto pracovní zkušenosti jednu po druhé 
vypíšeme pomocí cyklu `for` nebo `foreach`. Místo staticky vypsaného HTML 
kódu tedy vytvoříme cyklus, který bude procházet jednotlivé položky z 
pole `$pracovnizkusenosti` a bude je vypisovat.
4. Pokud máte hotovo, rozšíříme si výpis o počet let, které jsme v dané firmě 
pracovali.
5. Nejprve musíme rozšířit pole `$pracovnizkusenosti` o údaj počtu roků.
Jednorozměrné pole musíme předělat na dvourozměrné pole,
kde klíč bude název firmy a hodnota bude počet let.
```
[
    "nazev firmy" => 3
]
```
6. Nyní už můžeme upravit výpis tak,
aby jsme ho rozšířili i o výpis délky zkušenosti.
Z každé položky nového pole se získá firma a počet let strávených v této firmě.
Tyto informace spojte do formátu `{{firma}} ({{počet roků}} let)` a vložte 
do nadpisu našeho HTML bloku.
7. Z jednoduchého pole vytvoříme strukturované, aby jsme do něj mohli ukládat informace
lépe a později je rozšiřovat.
```
$pracovniZkusenosti = [
    [
        'jmeno' => 'PayPal',
        'delka' => 1,
        'popis' => 'Testoval jsem proces plateb.',
    ],
    [
        'jmeno' => 'Czechitas z.s.',
        'delka' => 4,
        'popis' => 'Programuji informační systém.',
    ],
];
```
8. Na základě nové struktury pole musíme upravit výpis.
7. Využijeme znalosti z domácího úkolu č. 2, kde jsme řešili podmíněné skloňování počtu let. 
Aby se nám s tím lépe pracovalo, uzavřeme kód do funkce. Vypadá takto a umístímě si ji zatím
na začátek souboru `config.php`:
```
function pocetLet($roky) {
  if ($roky == 1) {
    $text = 'rok';
  } elseif ($roky > 1 && $roky < 5) {
    $text = 'roky';
  } else {
    $text = 'let';
  }
  
  return $roky . ' ' .$text;
}
```
Funkce se použije tak, že zavoláme její jméno a předáme ji v kulatých závorkách parametry. 
V tomto případě je to počet let:
```
pocetLet(15);
```
Vrácenou hodnotu můžeme buďto vypsat přes `echo` nebo si ji přiřadit do proměnné, kteou později
použijeme. My ji použijeme v nadpisu pozice, kde teď máme v závorce napevno slovo `let`.


## Co dál?

1. Použijeme funkci `pocetLet` na lepší výpis délky práce nebo kdekoliv, kde pracujeme
s délkou v letech, takže i zpětně do textu `$oMne`. Nechceme svůj kód opakovat.
2. Rozšíříme pole `$pracovnizkusenosti` o další prvky: 
	- `od` - datum nástupu v anglickém formátu: `2018-08-01`),
	- `do` - datum ukočení v anglickém formátu, pokud nebude, tak místo hodnoty dej `null`
	- `nadrizeny` - pole, ktere bude obsahovat prvky `jmeno` a `prijmeni`
	- `kolegove` - pole, ktere bude obsahovat jmena kolegu. Buďto jako jednoduchý seznam
	textových hodnot nebo další pole, které bude obsahovat také prvky `jmeno` a `prijmeni`
3. Všechny hodnoty v poli hezky hezky vypíšeme v boxech, kde jsme dosud pracovali. Pokud
hrozí, že nějaká hodnota je `null`, je vhodné přidat podmínku. Např. tak, že pokud v prvku
`do` je `null`, tak se vypíše slovo `dosud`. "Ošklivým" formátem datumů se zatím zabývat nebudeme.
4. Dej si trochu práce s popisem pozic a výpisem všech údajů. Může se hodit informace, že kód do
sebe můžeš jakkoliv zanořovat. Není problém mít cyklus v cyklu, podmínky uvnitř cyklu apod.
5. Nahrej si do složky `images` všechny fotky, které máš připravené z domácího úkolu č. 1
6. První dvě fotky (čtvercová a na výšku) použij na místech, kde jsou teď použité původní fotky 
ze šablony. Chceme to dělat chytře, takže si vytvoř pro cestu k souboru proměnnou a tu potom použij
v HTML značce `img` v atributu `src`. Můžeš pak jednoduše měnit fotku přímo z `config.php`. Jak
vypadá cesta k obrázku se můžeš podívat v kterém koliv tagu `img`.
7. Připrav si úplně novou proměnnou obsahující pole. V poli si jednu položku pro zbylé fotografie
z domácího úkolu. U každé jsme si připravili název fotky a krátký popis a potřebujeme znát také cestu
k souboru, kde je fotka uložená.
8. Nově připravené pole vypiš cyklem v sekci **Our Works**. Připomenu postup:
	- najdi jaká část HTML kódu se bude opakovat cyklem (hodí se použít inspector prohlížeče)
	- vytvoř cyklus, který bude tu část HTML vypisovat
	- vhodně doplň hodnoty z pole
9. Na závěr bude fajn sekci **Our Works** už nějak lépe pojmenovat a krátký úvodní text taky napsat
po svém. Platí zase, že je dobré si ty texty dát do proměnné (minimálně název je použitý také v menu), 
abychom je mohly měnit na jednom místě.
