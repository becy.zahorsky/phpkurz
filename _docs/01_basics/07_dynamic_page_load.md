# Dynamické načítání stránky

## Úvod
Co budeme muset udělat, když budeme chtít vytvořit nějakou podstránku? Vytvořit nový soubor, vykopírovat do něj všechno z `index.php`: require, strukturu stránky, require jednotlivých částí a potom require samotné stránky. To není moc chytré.

## Úkol
1. Použijeme GET parametr, abychom si určili, jakou stránku zobrazit.
2. Vždy směřujeme uživatele na soubor `index.php` s různými hodnotami v GET parametru.
3. Zavoláme stránku s parametrem `page`: http://localhost/?page=homepage
4. V `$_GET['page']` máme teď hodnotu `homepage`
5. Vytvoříme nový soubor `page/layout.php`, který načteme na konci souboru `index.php` a přesuneme do něj úplně všechno kromě prvního PHP bloku.
6. Vytvoříme kód, který zavolá konkrétní controller podle toho, jakou stránku požadujeme.
7. Controller je něco, co nám chystá data potřebné pro danou stránku. Společná data necháváme v `config.php`
8. Vytvoříme první controller: `controller/homepage.php`
9. Jako první nastavíme proměnnou `$template`, pomocí které se rozhodneme v `layout.php`, jakou šablonu načíst.
10. Ošetříme mezní stavy.

## Procvičujeme

Vytvoř si ve složce `htdocs`, kde máš složku `mujweb` novou složku `cviceni`, do ktere si vytvor soubor `ukol1.php`. Do něj zpracuj zadání ze souboru `zadani.md` popsané zde: https://github.com/D3ryk/CzechitasPhpWorkshop/tree/master/1_Promenne_DatoveTypy_Operatory/ukol1

Když si nebudeš vědět rady, můžeš se podívat na možné řešení v sousedícím souboru.

Stejně tak si udělej soubor `ukol2.php` pro zadání 2: https://github.com/D3ryk/CzechitasPhpWorkshop/tree/master/2_Podminky/ukol2

atd. pro úkol 3: https://github.com/D3ryk/CzechitasPhpWorkshop/tree/master/3_Cykly/ukol3

a úkol 4: https://github.com/D3ryk/CzechitasPhpWorkshop/tree/master/4_Funkce/ukol4

Soubor spustíš v prohlížeči stejně jako svůj projekt, jen s uvedením jiné složky a konkrétního souboru: http://localhost/cviceni/ukol1.php