# Zobrazení základních informací o sobě

## Úvod

Máme přichystanou základní rozdělení webu do jednotlivých souborů.
Nyní se můžeme věnovat úpravám jednotlivých částí.

V této části si zkusíme nadefinovat pár proměnných a následně si je vypsat.
Budeme pracovat se soubory **index.php** a **header.php**

## Úkol
1. Na začátku souboru **index.php** (hned po příkazech na zapnutí chybových hlášek) vytvořte tři proměnné:
 - proměnnou obsahující titul osoby
 - proměnnou obsahující jméno
 - proměnnou obsahující příjmení
2. Dále vytvořte proměnnou, která spojí tyto tři proměnné do jednoho 
řetězce tak, aby po vypsání dával dohromady celé jméno i s titulem.
3. Tuto proměnnou vypište v souboru **index.php** do tagu `<h1></h1>`
a v souboru **header.php** do tagu s třídou `site-logo`.
Takto budete mít vypsané jméno na obou místech a pokud ho někdy budete chtít 
upravit, stačí pouze na jednom místě.
4. Vaším úkolem je vytvořit podmíněný blok v sekci **intro-section**.
Vytvořte proměnnou `$omne`, ve které bude nějaký text o vás. Poté druhou 
proměnnou `$zobrazOmne` s obsahem typu boolean (`true` nebo `false`).
5. Proměnnou `$omne` s textem o vás vypište za svoje jméno (za tag `<h1></h1>`) 
místo aktuálního odstavce, který se tam aktuálně nachází.
Zobrazení tohoto odstavce bude ale podmíněno proměnnou `$zobrazOmne`.
Pokud bude mít tato proměnná hodnotu `true`, zobrazí se tento odstavec.
V opačném případě se ostavec nezobrazí.

## Co dál? 

1. Není moc hezké mít proměnné, které se používají všude na stránce, na 
začátku souboru a muset je kopírovat, když budeme chtít vytvořit nějakou
podstránku. Pojďme vytvořit společný konfigurační soubor, kam budeme dávat
takové proměnné a nastavení (soubor `config.php` ve složce `config`).
2. Soubor `config.php` načteme na vhodném místě, tak aby jeho obsah byl dostupný "všude".
3. Najdi v projektu, kde všude je ještě původní název webu "Amplify" a nahraď ho vhodně nějakou z našich proměnných.
4. Hezké by byly iniciály vlevo nahoře v menu, kde teď máme celé jméno. Můžeme zkusit použít funkci na získání prvního znaku z textu. Zkus google, jak se taková funkce jmenuje a používá. Může ti pomoct také prezentace a tahák.
5. Připoj na konec textu v proměnné `$omne` novou větu, která bude vypadat takto: `Programuji s Czechitas už X let.` Místo `X` použij novou proměnnou do které dej číslo, kolik let už si z Czechitas. Vyzkoušej tam dát několik hodnot. Zjistíš, že formulace `... už 3 let`. Takže použijeme pár vhodných podmínek a to slovo pro roky/rok/let vypíšeme podle proměnné s počtem let. Výsledkem pro číslo 3 tedy bude: `Programuji s Czechitas už 3 roky.` apod. Využiješ k tomu if, elseif a else a vhodné operátory. Výsledek buďto rovnou vypiš nebo si ulož do proměnné, kterou ve větě využiješ. Buď kreativní :-)