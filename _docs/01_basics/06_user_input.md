# Uživatelský vstup

## Úvod
Pořádný dynamický web se neobejde bez uživatelského vstupu. Naše stránka
teď neumí žádné instrukce nebo data přijímat, tak si to půjdeme vyzkoušet.

PS: řekneme si rozdíl mezi `require` vs `include`
PPS: přesuneme překlady do vlastní složky `translate`, abychom po sobě uklidili z minulé lekce

## Úkol
1. Jako první dořešíme načítání jazykové verze z poslední lekce. Máme ve stránce přibližně takovýto kód:
```php
$cesky = false;

if ($cesky === true) {
  require 'translate/cs.php';
} else {
  require 'translate/en.php';
}
```
2. Místo manuálhího použití proměnné se rozhodneme pomocí hodnoty v `$_GET`
```php
$jazyk = $_GET['lang'];
require 'translate/' . $jazyk . '.php';
```
3. Jaké problémy může mít náš kód?
 - v GET nebude jazyk nastavený
 - nastavený jazyk nebude existovat
4. Chytře ošetříme:
```php
$jazyk = $_GET['lang'] ?? 'cs';

if (is_file('translate/' . $jazyk . '.php')) {
	require 'translate/' . $jazyk . '.php';
} else {
	require 'translate/cs.php';
}
```
ještě trochu lépe:
```php
$jazyk = $_GET['lang'] ?? 'cs';
$preklad = 'translate/' . $jazyk . '.php';

if (is_file($preklad)) {
	require $preklad;
} else {
	require 'translate/cs.php';
}
```
5. Vyzkoušíme adresu http://localhost/mujweb/?lang=en a měli bychom vidět anglickou verzi webu. Když zadáme např. `fr`, tak uvidíme českou, protože francouzkou mutaci nemáme vytvořenou.

## Co dál?
1. Vytvoř funkci `zjistiSouborSPreklady()`, kterou použijeme do konstrukce `require`, abychom neměli takto "komplikovaný" kód v `index.php` resp. `bootstrap.php`. Kód v indexu/bootstrapu by pak vypadal takto:
```php
require zjistiSouborSPreklady();
```
