# Nákupní seznam

## Úvod

Vytvoříme si jednoduchou stránku s nákupním seznamem. Může to být seznam
čehokoliv: knížek, filmů, oblečení.

Cílem je pracovat s formulářem, zpracovat POST požadavek, uložit si hodnoty
pro další použití a umět s němi pracovat.

## Úkol

1. Vytvoř nový prázdný PHP soubor `list.php` v hlavním adresáři.
2. Vlož do něj strukturu jako je v souboru `index.php` s tím rozdílem, že budeš
includovat soubor ne `page/homepage/base.php`, ale nový. Doporučuji vytvořit si
podobnou strukturu pro budoucí rozšiřitelnost.
3. Do stránky si vlož nějaký testovací text, aby nebyla úplně 
prázdná. (Lorem Impsum?)
3. Doplň menu o odkaz na tuto novou stránku.
4. Vyzkoušej, že ti stránka funguje.