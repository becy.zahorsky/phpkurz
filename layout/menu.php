<nav class="site-navigation position-relative text-right" role="navigation">
    <ul class="site-menu main-menu js-clone-nav mr-auto d-none d-lg-block">
        <li><a href="#home-section" class="nav-link">Home</a></li>
        <li><a href="#work-section" class="nav-link">Work</a></li>
        <li><a href="#process-section" class="nav-link">Process</a></li>
        <li><a href="#services-section" class="nav-link">Services</a></li>
    </ul>
</nav>
