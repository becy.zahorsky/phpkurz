<?php

define('WEB_TITLE', 'Můj web');

function webTitle() {
    echo WEB_TITLE;
}

function t(string $key) {
    global $locale;

    if (array_key_exists($key, $locale)) {
        return $locale[$key];
    } else {
        return $key;
    }
}

