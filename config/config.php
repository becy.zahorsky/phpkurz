<?php

// vypisovani chybovych hlasek
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once 'locale/cs.php';

// nazev celeho webu
$webName = 'JanZa';

$title = 'Bc.';
$firstName = 'Jan';
$lastName = 'Záhorský';

// jakoze moc psani
$fullName = $title . ' ' . $firstName . ' ' . $lastName;

// lepsi
$fullName = "$title $firstName $lastName";

// uplne OK, protoze promenna muze byt cokoliv
$fullName = "{$title} {$firstName}&nbsp;{$lastName}";
